function getBoard() {
  return new Promise((resolve, reject) => {
    let board
    console.log('Fetching board...');
    setTimeout(function() {
    try{    
      board = {
          id: "def453ed",
          name: "Thanos"
        };
        console.log('Received board');
    } catch(err){
      reject(err)
    }
    resolve(board);
    }, 1000);
  });
}

function getLists(boardId) {
  return new Promise((resolve, reject) => {
    let lists
    console.log(`Fetching lists for board id ${boardId}...`);
    setTimeout(function() {
    try{
      lists = {
          def453ed: [
            {
              id: "qwsa221",
              name: "Mind"
            },
            {
              id: "jwkh245",
              name: "Space"
            },
            {
              id: "azxs123",
              name: "Soul"
            },
            {
              id: "cffv432",
              name: "Time"
            },
            {
              id: "ghnb768",
              name: "Power"
            },
            {
              id: "isks839",
              name: "Reality"
            }
          ]
        };
        console.log(`Received lists for board id ${boardId}`);
    } catch(err){
      reject(err)
    }
    resolve(lists[boardId]);
    }, 500);
  });
}

function getCards(listId) {
  return new Promise((resolve, reject) => {
    let cards
    console.log(`Fetching cards for list id ${listId}...`);
    setTimeout(function() {
      try{
        cards = {
        qwsa221: [
          {
            id: "1",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          },
          {
            id: "2",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          },
          {
            id: "3",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          }
        ],
        jwkh245: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "3",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "4",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        azxs123: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        cffv432: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        ghnb768: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ]
      };
      console.log(`Received cards for list id ${listId}`);
    } catch(err){
      reject(err)
    }
    resolve(cards[listId] || []);
    }, 1500);
  });
}

/**
 * this function takes a Array as input and contains the cardid which we need to display if we want all the values then just send an empty Array
 * @param {Array} arr 
 */ 

function getValues(arr){

  if(Array.isArray(arr)){

    getBoard()
    .then((a) => {return getLists(a.id)})
    .then((a) => {
      return a.filter(function(va){
        if(arr.length === 0){
          return true
        }else{
          return arr.filter(function(va2){return va2 == va.id}).length !==0
        }
      }
      ).map(function(va){
        return va.id
      })
    })
    .then((a)=> {
      a.map(element => getCards(element)
      .then((c)=>console.log(c)).catch((c)=>console.log(c + 'Not in cards')))
      
    })
    .catch((err)=>console.log(err))
  }else {
    console.log('Arguments must be an array')
  }
}
// Task 1 board -> lists -> cards for list qwsa221
getValues(['qwsa221'])
// Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously
getValues(['qwsa221','jwkh245'])
// Task 3 board -> lists -> cards for all lists simultaneously
getValues([])