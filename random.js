function fetchRandomNumbers(){
    return new Promise((resolve, reject) => {
        let randomNum
        console.log('Fetching number...');
        setTimeout(() => {
        try{
            randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
        } catch(err){
            reject(err)
        }
        resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString(){
    return new Promise((resolve, reject) => {
        console.log('Fetching string...');
        let result
        setTimeout(() => {
            try{
                result           = '';
                let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                let charactersLength = characters.length;
                for ( let i = 0; i < 5; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }
                console.log('Received random string:', result);
            } catch(err){
                reject(err)
            }
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}


// task 1
fetchRandomNumbers().then((randomNum) => {
    console.log(randomNum)
});
fetchRandomString().then((randomNum) => {
    console.log(randomNum)
});
// task 2

//let sum = 0
fetchRandomNumbers()
    .then(async(randomNum) => {
        let sum = randomNum
        console.log('sum = '+sum)
        sum += await fetchRandomNumbers()
        console.log('sum = '+sum)
    })
    .catch((err)=>console.log(err))
//task 3
let va = ''
fetchRandomNumbers()
    .then(async(randomNum) => {
        va += randomNum
        va += await fetchRandomString()
        console.log('Concatenated String = '+va)
    })
    .catch((err)=>console.log(err))
//task 4
fetchRandomNumbers()
    .then(async(randomNum) => {
        let sum = randomNum
        for(let index = 0; index< 9;index++){
            sum += await fetchRandomNumbers()
        } 
        console.log(`sum of 10 = ${sum}`)
    })
    .catch((err)=>console.log(err))
